---
eventid: techtue508
startdate:  2019-02-12
starttime: "19:00"
linktitle: "TechTue 509"
title: "TechTuesday 509"
price: ""
image: "techtuesday.png"
series: "TechTuesday"
eventtype: "Social get-together"
location: "HSBXL"
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
