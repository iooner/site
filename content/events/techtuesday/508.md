---
eventid: techtue508
startdate:  2019-02-05
starttime: "19:00"
linktitle: "TechTue 508"
title: "TechTuesday 508"
price: ""
image: "techtuesday.png"
series: "TechTuesday"
eventtype: "Social get-together"
location: "HSBXL"
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
