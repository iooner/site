---
startdate:  2019-11-09
starttime: "14:30"
endtime: "17:00"
linktitle: "Tech met kids"
title: "Tech met kids"
price: ""
image: ""
series: "Tech With Kids"
eventtype: "for kids age 6+"
location: "HSBXL"
---

Tech With Kids is a get together of kids with parents to work on projects, learn new things, play with tech.

It's an informal gathering, we speak the language of the kids (Dutch, French, English, ...)