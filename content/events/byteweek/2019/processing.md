---
startdate:  2019-01-29
starttime: "17:00"
enddate:  2019-01-29
linktitle: "Processing Community Day Brussels"
title: "Processing Community Day Brussels"
location: "HSBXL"
eventtype: "Workshop"
price: "Free"
series: "byteweek2019"
image: "PCD2019.svg"
--- 

Join Processing Community Day to Celebrate Art, Code, and Diversity
 
PCD @ Brussels is an inclusive event that will bring together people of all ages to celebrate and explore art, code, and activism. Join us for two hours of initiation of creative coding using processing at HSBXL during byteweek.

After an introduction to the possibilities of creative coding by Frederik Vanhoutte, (https://wblut.com/) (TBC) we will run a short workshop on p5js, a web editor to create art via code.
Please bring your computer and create an account on https://editor.p5js.org/ .
 
Processing is a free and open-source software platform for code within the context of the visual arts, created by Casey Reas and Ben Fry. It is complemented by a web version, called p5.js, created by Lauren McCarthy. To date, Processing and p5.js are used by a worldwide community of artists, coders, educators, and students.
Additional information on Processing Community Day can be found at day.processing.org.